<?php

namespace Drupal\custom_configuration\ParamConverter;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Symfony\Component\Routing\Route;
use Drupal\node\Entity\Node;

class CustomParamConverter implements ParamConverterInterface {

  /**
   * Converts path variables to their corresponding objects.
   *
   * @param type $value
   *   The raw value.
   * @param type $definition
   *   The parameter definition provided in the route options.
   * @param type $name
   *   The name of the parameter.
   * @param array $defaults
   *   The route defaults array.
   *
   * @return string
   */
  public function convert($value, $definition, $name, array $defaults) {

    $configFactory = \Drupal::config('system.site');
    if ($configFactory->get("siteapikey") && $configFactory->get("siteapikey") == $value) {
      return $configFactory->get("siteapikey");
    }
    $node = Node::load($value);
    if ($node && $node->getType() == 'page') {
      return $node;
    }
    return 'Access Denied';
  }

  /**
   * Determines if the converter applies to a specific route and variable.
   *
   * @param type $definition
   *   The parameter definition provided in the route options.
   * @param type $name
   *   The name of the parameter.
   * @param Route $route
   *   Route Information.
   *
   * @return boolean
   */
  public function applies($definition, $name, Route $route) {
    if ($definition['type']) {
      if ($definition['type'] == 'node' || $definition['type'] == 'api') {
        return TRUE;
      }
    }
  }

}
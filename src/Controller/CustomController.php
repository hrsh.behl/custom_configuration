<?php

namespace Drupal\custom_configuration\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Class CustomController.
 *
 * @package Drupal\custom_configuration\Controller
 */
class CustomController extends ControllerBase {

  /**
   * Function to return Json Response.
   *
   * @param type $api
   *   Api Data.
   * @param type $node
   *   Node Object.
   *
   * @return JsonResponse
   */
  public function jsonResponse($api = NULL, $node = NULL) {
    $message = T('Access Denied');
    if ($api == $message || $node == $message) {
      return new JsonResponse(
          ['message' => $message,
        'code' => 200,
          ]
      );
    }
    return new JsonResponse(
        ['node' => $node->toArray(),
      'code' => 200,
        ]
    );
  }

}